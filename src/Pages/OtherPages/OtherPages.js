import React from "react";
import Strings from "../../assets/Locales/locales"
const OtherPages = () => {
    return ( 
        <div style={{ textAlign: "center", fontWeight: "bolder", color: "#dc3545", marginTop:"20px"}}>
            {Strings.PleaseNavigate}
        </div>
    )
}
export default OtherPages;