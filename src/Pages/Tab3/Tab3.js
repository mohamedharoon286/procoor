import React, { Component } from "react";
import Spinner from "../../components/UI/Spinner/Spinenr"
import Input from "../../components/UI/Input/Input"
import { RadioButton, Button } from "../../components/UI/buttons/Button";
import Strings from "../../assets/Locales/locales";
import Tab3Classes from "./Tab.module.scss";
import Table from "../../components/UI/Table/table";
class Tab3 extends Component {
    constructor() {
        super();
        this.state = {
            Data: [],
            Subject: null,
            Num: null,
            Status: null,
            Name: null,
            Loading: false,
        }
    }
    SubjectHandler = (e) => {
        this.setState({
            Subject: e.target.value,
        })
    }
    StatusHandler = (e) => {
        this.setState({
            Status: e.target.value,
        })
    }
    NumHandler = (e) => {
        this.setState({
            Num: e.target.value,
        })
    }
    NameHandler = (e) => {
        this.setState({
            Name: e.target.value,
        })
    }
    handleFormSubmit = (e) => {
        e.preventDefault();
        let FormData = {
            Subject: this.state.Subject,
            Name: this.state.Name,
            Num: this.state.Num,
            Status: this.state.Status,
        }
        let NewData = this.state.Data.slice();
        NewData.push(FormData)
        // Here We Should Make the Request .
        this.setState({
            Data: NewData,
            Loading: false,
            Subject: null,
            Num: null,
            Status: null,
            Name: null,
        })
        let Inputs = document.getElementsByTagName("input");
        for (let i of Inputs) {
            i.value = "";
        }
    }
    render() {
        Strings.setLanguage(this.props.lang)
        console.log("the New Data Array", this.state.Data)
        return (
            <div className={Tab3Classes.Tab3Container}>
                <form className={Tab3Classes.Form} onSubmit={this.handleFormSubmit}>
                        <div className={Tab3Classes.SubjectContainer}>
                            <Input label={Strings.Subject} onChange={this.SubjectHandler} />
                        </div>
                        <div onChange={this.StatusHandler} className={Tab3Classes.StatusContainer}>
                            <span>{Strings.Status}</span>
                            <label><RadioButton value="true" name="UserStatus" />{Strings.Opened}</label>
                            <label><RadioButton value="false" name="UserStatus" />{Strings.Closed}</label>
                        </div>
                    <div className={Tab3Classes.NumContainer}>
                        <Input label={Strings.Num} onChange={this.NumHandler} name="UserNum" />
                    </div>
                    <div className={Tab3Classes.NameContainer}>
                        <Input label={Strings.Name} onChange={this.NameHandler} name="UserName" />
                    </div>
                    <div className={Tab3Classes.ButtonContainer}>
                        <Button type="submit" color="Primary"> {Strings.Add}</Button>
                        {this.state.loading ? <Spinner /> : null}
                    </div>
                </form>
                <h2 className={Tab3Classes.Previous}>{Strings.PreviousCycles}</h2>
                {this.state.Data.length > 0 ?<Table Data = {this.state.Data}/> : null }
            </div>
        )
    }
}
export default Tab3;