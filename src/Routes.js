import React from 'react';
import Loadable from 'react-loadable';
import Spinner from "./components/UI/Spinner/Spinenr"
function LoadingFunction() {
    // The Routing Here is Too Fast Because there is no Request Made.
    // you will notice it once you make Requests from your Server.
    return (
        <Spinner/>
    );
}
const Form = Loadable({
    loader: () => import('./Pages/Tab3/Tab3'),
    loading: LoadingFunction,
});
const OtherTabs = Loadable({
    loader: () => import("./Pages/OtherPages/OtherPages"),
    loading: LoadingFunction,
});
const Routes = [
    { path: '/tab1', exact: true, name: 'tab1', component: OtherTabs },
    { path: '/tab2', exact: true, name: 'tab2', component: OtherTabs },
    { path: '/tab3', exact: true, name: 'tab3', component: Form },
    { path: '/tab4', exact: true, name: 'tab4', component: OtherTabs },
    { path: '/lastproject', exact: true, name: 'lastproject', component: OtherTabs },
]
export default Routes;