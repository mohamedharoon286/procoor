import LocalizedStrings from 'react-localization';
let Strings = new LocalizedStrings({
  en: {
    // First Side Menu Routes .
    BackToProjects: "Back to Projects",
    Tab1: "Tab 1 ",
    Tab2: "Tab 2",
    Tab3: "Tab 3",
    Tab4: "Tab 4",
    // end of Side Menu Routes.
    TechnicalOffice: "Technical Office",
    Submittal: "Submittal",
    address: "Uptown Cairo ",
    // OtherPages .
    PleaseNavigate: "Please Navigate to tab 3",
    // The Form .
    Subject: "Subject",
    Status: "Status",
    Opened: "Opened",
    Closed: "Closed",
    Num: "No.",
    Add: "Add Item",
    Name: 'Name',
    PreviousCycles : "Previous Cycles"
  },
  ar: {
    // First Side Menu Routes .
    BackToProjects: "العوده الي المشاريع",
    Tab1: "تابه 1",
    Tab2: "تابه2", 
    Tab3: "تابه3",
    Tab4: "تابه4",
    // end of Side Menu Routes.
    TechnicalOffice: " المكتب التقني",
    Submittal: "المقدمه",
    address: "القاهره",
    PleaseNavigate: "من فضلك انتقل الي علامه التبويب الثالثه",
    // The Form .
    Subject: "الموضوع",
    Status: "الحاله",
    Opened: "مفتوح",
    Closed: "مغلق",
    Num: "رقم",
    Add: "أضف عنصر",
    Name: "الاسم",
    PreviousCycles : "البيانات السابقه"
  }
});
export default Strings;