import React from "react";
import HeaderClasses from "./Header.module.scss";
import { Button } from "../../UI/buttons/Button";
import Strings from "../../../assets/Locales/locales";
const Header = (props) => {
    Strings.setLanguage(props.lang)
    return (
        <div className={HeaderClasses.Header}>
            <div className={HeaderClasses.ChangeLanguageContainer}>
                <div>
                <Button onClick={props.onClickHandler.bind(this , "ar")} color = {props.lang === "ar" ? "Primary" : "Default"}>للغه العربيه</Button>
                <Button onClick={props.onClickHandler.bind(this , "en")} color = {props.lang === "en" ? "Primary" : "Default"}>English</Button>
                </div>
                <h1 className = {HeaderClasses.LastProject}> {Strings.TechnicalOffice + props.LastProject}</h1>
            </div>
            <div className={HeaderClasses.SubHeader}>
                <h2>{Strings.Submittal}</h2>
                <p className = {HeaderClasses.Address}>{Strings.address + props.LastProject}</p>
            </div>
        </div>
    )
}
export default Header; 