import React, { PureComponent } from "react";
import LayoutClasses from "./Layout.module.scss";
import Strings from "../../../assets/Locales/locales"
import SideMenu from "../SideMenu/sideMenu"
import Routes from "../../../Routes";
import Header from "../Header/Header"
import {Route, Switch, BrowserRouter } from "react-router-dom";
class Layout extends PureComponent {
  constructor() {
    super();
    this.state = {
      lang: "en",
      LastProject: "East Town ( P2 )"
    }
  }
  
  // Put the Lang in the Local Storage .
  ChangeLanguageHandler = (NewLang) => {
    if (this.state.lang !== NewLang) {
      this.setState({
        lang: NewLang
      }, () => Strings.setLanguage(this.state.lang))
    }
  }
  render() {
    Strings.setLanguage(this.state.lang)
    if (this.state.lang === 'ar') {
      document.body.style.direction = "rtl";
    }
    else if (this.state.lang === 'en') {
      document.body.style.direction = "ltr";
    }
    return (
      <BrowserRouter>
        <SideMenu lang={this.state.lang} LastProject={this.state.LastProject} />
        <div className={LayoutClasses.MainBody}>
          <Header onClickHandler={this.ChangeLanguageHandler} lang={this.state.lang} LastProject={this.state.LastProject} />
        <Switch>
            {Routes.map(route => {
              return route.component ? (
                <Route
                  key={route.name}
                  path={route.path}
                  exact="exact"
                  name={route.name}
                  render={props=> <route.component {...props} lang = {this.state.lang}/>}
                />
              ) : null;
            })}
          </Switch>
        </div>
      </BrowserRouter>
    )
  }
}
export default Layout;