import Strings from "../../../assets/Locales/locales"
console.log("the Strings", Strings.tab1);
export const RoutesEn = [
    {
        name: "Tab 1 ",
        icon: "fas fa-th",
        url : "tab1", 
    },
    {
        name: "Tab 2 ",
        icon: "fas fa-home",
        url : "tab2", 
    },
    {
        name: "Tab 3 ",
        icon: "fas fa-city",
        url : "tab3", 
    },
    {
        name: "Tab 4",
        icon: "far fa-copy",
        url : "tab4", 
    }
]
export const RoutesAr = [
    {
        name: "تابه 1 ",
        icon: "fas fa-th",
        url : "tab1", 
    },
    {
        name: "تابه2",
        icon: "fas fa-home",
        url : "tab2", 
    },
    {
        name: "تابه3",
        icon: "fas fa-city",
        url : "tab3", 
    },
    {
        name: "تابه4",
        icon: "far fa-copy",
        url : "tab4", 
    }
]
