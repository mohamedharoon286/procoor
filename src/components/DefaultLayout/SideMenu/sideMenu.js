import React from "react";
import Classes from "./SideMenu.module.scss";
import {RoutesEn , RoutesAr} from "./SideMenuRouts";
import { Link } from "react-router-dom";
import Logo from "../../../assets/Images/Logo.png";
import { Button } from "../../UI/buttons/Button";
import Strings from "../../../assets/Locales/locales";
const SideMenu =(props)=> {
  let Routes = null;
  if (props.lang === "en") {
    Routes = RoutesEn;
  }
  else {
    Routes = RoutesAr;
  }
    Strings.setLanguage(props.lang)
    return (
      <aside className={Classes.SideMenu}>
        {/* Starting The Logo */}
        <div className={Classes.LogoContainer}>
          <img src={Logo} className={Classes.Image} alt = "Logo"/>
          <div className={Classes.BtnContainer}><Button color = "Primary"> PM </Button></div>
        </div>
        {/* End of the Logo */}
        <div className={Classes.Route}>
          {/* All these Links should have a to prop which indecates the page they are referring to . */}
          {/*  */}
          <Link to = {`/lastProject`}>
            <i className="fas fa-briefcase"></i>
            <span className={Classes.Small}>{"< " + Strings.BackToProjects}</span>
            {/* this is the Place Holder For the Last Porject . */}
            <p className = {Classes.projectName}>{props.LastProject}</p>
          </Link>
        </div>
        {/* Start of the Side Menu Routes */}
          {Routes.map((element) => {
            return (
              <div key={element.name + element.icon} className={Classes.Route} >
                <Link to = {`/${element.url}`}>
                  <i className={element.icon}></i>
                  <span>{element.name}</span>
                </Link>
              </div>
            )
          })}
        {/* End of the Side Menu  */}
      </aside>
    );
}
export default SideMenu;