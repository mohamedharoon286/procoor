import React from "react";
import InputClasses from "./Input.module.scss";
import Strings from "../../../assets/Locales/locales";
const Input = (props) => {
    // Strings.setLanguage(props.lang)
    return (
        <div className={InputClasses.InputContainer}>
            <label>{props.label}</label>
            <input className = {InputClasses.Input} {...props} placeholder ={props.label}/>
        </div>
    )
}
export default Input;