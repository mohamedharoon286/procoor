import React from "react";
import SpinnerClasses from "./Spinner.module.scss";
const Spinner = () => {
    return <div className = {SpinnerClasses.Loader}></div>
}
export default Spinner;