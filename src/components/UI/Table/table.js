import React, { Component } from 'react';
import Strings from '../../../assets/Locales/locales'
import TableClasses from './Table.module.scss';
const Table = (props) => {
    let TableData = props.Data.map((element, index) => {
        return (
            <tr key ={index}>
                <td>{element.Num}</td>
                <td>{element.Subject}</td>
                <td>{element.Status}</td>
                <td>{element.Name}</td>
            </tr>
        )
    })
    return (
        <div className={TableClasses.TableContainer}>
            <table className={TableClasses.Table}>
                <colgroup span={1} className={TableClasses.Span1} />
                <colgroup span={1} className={TableClasses.Span2} />
                <thead>
                    <tr className={TableClasses.TableRow}>
                        <th>{Strings.Num}</th>
                        <th>{Strings.Subject}</th>
                        <th>{Strings.Status}</th>
                        <th>{Strings.Name}</th>
                    </tr>
                </thead>
                <tbody>
                    {TableData}
                </tbody>
            </table>
        </div>
    )
}

export default Table;
