import React from 'react';
import BtnClasses from "./Button.module.scss";
export const Button = (props) => {
    let AppliedClasses = [BtnClasses.Button];
    if (props.color === "Primary") {
        AppliedClasses.push(BtnClasses.Primary)
    }
    else if (props.color === "Default") {
        AppliedClasses.push(BtnClasses.Default);
    }
    return (
        <button {...props} className={AppliedClasses.join(" ")}> {props.children} </button>
    );
}
export const RadioButton = (props) => {
    return (
        <input type="radio" {...props} className ={BtnClasses.Radio}/>
    )
}